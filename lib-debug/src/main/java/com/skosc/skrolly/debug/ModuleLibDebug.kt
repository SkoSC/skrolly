package com.skosc.skrolly.debug

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.skosc.skrolly.lib.android.ApplicationPlugin
import com.skosc.skrolly.lib.di.DefaultModule
import okhttp3.Interceptor
import org.kodein.di.generic.bind
import org.kodein.di.generic.inSet
import org.kodein.di.generic.singleton

val ModuleLibDebug = DefaultModule("lib-debug") {
    bind<Interceptor>().inSet() with singleton { StethoInterceptor() }
    bind<ApplicationPlugin>().inSet() with singleton { StethoSetupPlugin() }
}