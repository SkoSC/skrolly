package com.skosc.skrolly.debug

import android.app.Application
import com.facebook.stetho.Stetho
import com.skosc.skrolly.lib.android.ApplicationPlugin

class StethoSetupPlugin : ApplicationPlugin {
    override fun onCreate(app: Application) {
        Stetho.initializeWithDefaults(app)
    }
}