package com.skosc.skrolly.core

/**
 * Throws [IllegalStateException] with specified [message] when called on null object, or returns it otherwise
 */
fun <T> T?.orThrow(message: String): T = when(this) {
    null -> throw IllegalStateException(message)
    else -> this
}

