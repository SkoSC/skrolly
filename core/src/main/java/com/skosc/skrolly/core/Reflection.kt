package com.skosc.skrolly.core

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

inline fun <reified T> typeOf(): Type {
    return object : TypeToken<T>() {}.type
}