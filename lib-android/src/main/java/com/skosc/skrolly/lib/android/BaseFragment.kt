package com.skosc.skrolly.lib.android

import androidx.fragment.app.Fragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

open class BaseFragment : Fragment(), KodeinAware {
    override val kodein: Kodein by lazy { (requireActivity().application as KodeinAware).kodein }
}