package com.skosc.skrolly.lib.android

import android.app.Application

interface ApplicationPlugin {
    fun onCreate(app: Application) = Unit
    fun onTerminate(app: Application) = Unit
}