package com.skosc.skrolly.domain.images

import com.skosc.skrolly.domain.images.entitiy.Image
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType

class ImagePagedDataSourceContainer(private val dataSources: Set<ImagePagedDataSource>) {
    fun forType(providerType: ImageProviderType): ImagePagedDataSource {
        return dataSources.firstOrNull { it.assignedImageProviderType == providerType }
            ?: error("Can't find ${ImagePagedDataSource::class.java.simpleName} for type=$providerType")
    }

}