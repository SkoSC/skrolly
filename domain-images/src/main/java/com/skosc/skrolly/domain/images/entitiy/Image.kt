package com.skosc.skrolly.domain.images.entitiy

interface Image {
    val url: String
    val width: Int
    val height: Int
}