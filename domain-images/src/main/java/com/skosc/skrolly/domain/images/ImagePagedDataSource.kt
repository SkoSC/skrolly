package com.skosc.skrolly.domain.images

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.skosc.skrolly.domain.images.entitiy.*

interface ImagePagedDataSource {
    suspend fun createImagePagedList(node: ProviderNodeId): LiveData<PagedList<Image>>
    val assignedImageProviderType: ImageProviderType
}