package com.skosc.skrolly.domain.images.misc

import com.google.gson.JsonDeserializer
import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import java.lang.reflect.Type

typealias ImageProviderNodeDeserializerPack = Pair<ImageProviderType, ImageProviderNodeDeserializer>

interface ImageProviderNodeDeserializer : JsonDeserializer<ImageProviderNode> {
    val type: Type
}