package com.skosc.skrolly.domain.images.entitiy

interface ImageProviderNode {
    val id: String
    val title: String
}