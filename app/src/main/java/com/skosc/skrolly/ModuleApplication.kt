package com.skosc.skrolly

import com.skosc.skrolly.lib.android.ApplicationPlugin
import com.skosc.skrolly.lib.di.DefaultModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.setBinding

val ModuleApplication = DefaultModule("application") {
    bind() from setBinding<ApplicationPlugin>()
}
