package com.skosc.skrolly.lib.mvvm.coroutines

import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlin.reflect.KProperty

// TODO Rewrite to be async
class CoroutineLiveDataPropertyDelegate<T>(private val scope: CoroutineScope, private val provider: suspend () -> T) {
    private var holder: T? = null

    @Synchronized
    operator fun getValue(thisRef: Any?, prop: KProperty<*>): LiveData<T> {
        return scope.launchLiveData {
            if (holder == null) {
                holder = provider.invoke()
            }

            return@launchLiveData holder!!
        }
    }
}

fun <T> launchLiveDataDelgate(scope: CoroutineScope, provider: suspend () -> T): CoroutineLiveDataPropertyDelegate<T> =
    CoroutineLiveDataPropertyDelegate(scope, provider)
