package com.skosc.skrolly.feature.menu

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.skosc.skrolly.lib.android.SimpleDiffCallback
import com.skosc.skrolly.lib.uikit.manipulateColor

class MenuRecyclerAdapter(context: Context, private val onItemClick: (MenuItemUiObject) -> Unit) :
    RecyclerView.Adapter<MenuRecyclerAdapter.ViewHolder>() {
    companion object {
        private const val DEFAULT_ANIMATION_DURATION: Long = 100L
    }

    private val asyncDiff = AsyncListDiffer<MenuItemUiObject>(this, SimpleDiffCallback())
    private val colorResolver = TileColorResolver.from(context, R.array.feature_menu_item_color)

    fun submitItems(uiObjects: List<MenuItemUiObject>) {
        asyncDiff.submitList(uiObjects)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.feature_menu_item_menu_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val uiObject = asyncDiff.currentList[position]

        holder.title.text = uiObject.title
        val resolvedColor = colorResolver.resolve(uiObject, position)

        holder.container.background = ColorDrawable(resolvedColor)
        holder.container.setOnClickListener { view ->
            view.animate()
                .scaleX(0.8f)
                .scaleY(0.8f)
                .setDuration(DEFAULT_ANIMATION_DURATION)
                .setInterpolator(AccelerateInterpolator())
                .withEndAction {
                    view.animate()
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(DEFAULT_ANIMATION_DURATION)
                        .setInterpolator(AccelerateInterpolator())
                        .withEndAction { onItemClick.invoke(uiObject) }
                        .start()
                }
                .start()
        }
    }

    override fun getItemCount(): Int = asyncDiff.currentList.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.title)
        val container: ViewGroup = view.findViewById(R.id.container)
    }
}