package com.skosc.skrolly.feature.menu

import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.domain.images.entitiy.ProviderNodeId

data class MenuItemUiObject(
    val title: String,
    val providerType: ImageProviderType,
    val nodeId: ProviderNodeId
)