package com.skosc.skrolly.feature.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skosc.skrolly.lib.android.BaseFragment
import com.skosc.skrolly.lib.mvvm.viewModelProvider
import kotlinx.android.synthetic.main.feature_menu_fragment_menu.*


class MenuFragment : BaseFragment() {
    private val vm by viewModelProvider(MenuViewModel::class)
    private val navController by lazy { findNavController() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.feature_menu_fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val adapter = MenuRecyclerAdapter(requireContext(), ::onMenuItemClicked)
        recycler.adapter = adapter
        vm.menuItems.observe(this, Observer(adapter::submitItems))

    }

    private fun onMenuItemClicked(model: MenuItemUiObject) {
        navController.navigate(
            R.id.destination_scroller, bundleOf(
                "providerType" to model.providerType,
                "node" to model.nodeId
            )
        )
    }
}