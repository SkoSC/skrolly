package com.skosc.skrolly.feature.menu

import android.content.Context
import androidx.annotation.ArrayRes
import androidx.annotation.ColorInt
import kotlin.math.abs
import kotlin.math.min

class TileColorResolver(colors: IntArray) {
    private val shuffledColors = colors.sorted()

    @ColorInt
    fun resolve(target: Any, offset: Int): Int {
        val targetHash = target.hashCode()
        val arrayPinter = (abs(targetHash) + offset * 3) % shuffledColors.size
        return shuffledColors[min(arrayPinter, shuffledColors.size - 1)]
    }

    companion object {
        fun from(context: Context, @ArrayRes resource: Int): TileColorResolver {
            val colors = context.resources.getIntArray(resource)
            return TileColorResolver(colors)
        }
    }
}
