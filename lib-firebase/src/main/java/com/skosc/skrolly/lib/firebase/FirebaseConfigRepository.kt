package com.skosc.skrolly.lib.firebase

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.skosc.skrolly.lib.config.ConfigurationRepository
import timber.log.Timber
import java.lang.reflect.Type
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.reflect.KClass

class FirebaseConfigRepository(
    private val remoteConfig: FirebaseRemoteConfig,
    private val gson: Gson
) : ConfigurationRepository {
    companion object {
        private val FETCH_TIMEOUT: Long = if (BuildConfig.DEBUG) 0 else 60 * 60
    }

    override suspend fun <T : Any> get(resource: String, type: Type): T {
        return suspendCoroutine { continuation ->
            remoteConfig.fetch(FETCH_TIMEOUT).addOnCompleteListener { task ->
                Timber.i("Firebase RemoteConfig fetch status: ${task.isSuccessful}")

                if (task.isSuccessful) {
                    remoteConfig.activate()
                } else {
                    Timber.e(task.exception)
                }

                val json = remoteConfig.getString(resource)
                val tObj = gson.fromJson<T>(json, type)
                continuation.resume(tObj)
            }
        }
    }
}
