package com.skosc.skrolly.lib.firebase

import android.app.Application
import com.google.firebase.FirebaseApp
import com.skosc.skrolly.lib.android.ApplicationPlugin

class FirebaseApplicationPlugin : ApplicationPlugin {
    override fun onCreate(app: Application) {
        FirebaseApp.initializeApp(app)
    }
}