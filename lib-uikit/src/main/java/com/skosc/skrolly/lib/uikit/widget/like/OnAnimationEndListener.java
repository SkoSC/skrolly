package com.skosc.skrolly.lib.uikit.widget.like;

public interface OnAnimationEndListener {
    void onAnimationEnd(LikeButton likeButton);
}
