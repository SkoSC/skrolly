package com.skosc.skrolly.lib.uikit.widget.like;

/**
 * Created by Joel on 23/12/2015.
 */
public enum IconType {
    Heart,
    Thumb,
    Star
}
