package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName
import com.skosc.skrolly.domain.images.entitiy.Image

data class RedditImage(
    @SerializedName("url") val apiUrl: String,
    @SerializedName("width") override val width: Int,
    @SerializedName("height") override val height: Int
) : Image {
    override val url: String
        get() = apiUrl.replace("amp;", "")
}