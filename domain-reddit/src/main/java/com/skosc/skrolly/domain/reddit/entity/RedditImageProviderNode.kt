package com.skosc.skrolly.domain.reddit.entity

import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode

data class RedditImageProviderNode(
    override val id: String,
    override val title: String,
    private val url: String
) : ImageProviderNode