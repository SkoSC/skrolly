package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName

data class RedditImagePack(
    @SerializedName("source") val source: RedditImage
)