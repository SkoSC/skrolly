package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName

data class RedditResponseData<T>(
    @SerializedName("children") val children: List<RedditChildWrapper<T>>,
    @SerializedName("after") val after: String?,
    @SerializedName("before") val before: String?
)