package com.skosc.skrolly.domain.reddit.repository

import com.skosc.skrolly.domain.reddit.entity.RedditPost
import com.skosc.skrolly.domain.reddit.entity.RedditResponseData
import com.skosc.skrolly.domain.reddit.service.RedditPostService

class RedditPostRepository(private val redditPostService: RedditPostService) {
    suspend fun loadPosts(
            subReddit: String,
            limit: Int, before: String? = null,
            after: String? = null
    ): RedditResponseData<RedditPost> {
        val data = redditPostService.posts(subReddit, limit, after = after, before = before).body()!!.data
        val newChildren = if (data.children.size > limit) {
            data.children.subList(data.children.size - limit, limit)
        } else {
            data.children
        }


        return data.copy(children = newChildren)
    }
}