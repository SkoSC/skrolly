package com.skosc.skrolly.domain.reddit.service

import com.skosc.skrolly.domain.reddit.entity.RedditPost
import com.skosc.skrolly.domain.reddit.entity.RedditResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RedditPostService {
    @GET("/r/{sub_reddit}/.json")
    suspend fun posts(
            @Path("sub_reddit") subReddit: String,
            @Query("limit") limit: Int? = 25,
            @Query("after") after: String? = null,
            @Query("before") before: String? = null
    ): Response<RedditResponse<RedditPost>>
}