package com.skosc.skrolly.domain.reddit.model

import androidx.paging.ItemKeyedDataSource
import com.skosc.skrolly.domain.reddit.entity.RedditPost
import com.skosc.skrolly.domain.reddit.repository.RedditPostRepository
import kotlinx.coroutines.*

class RedditPostDataSource(
    coroutineScope: CoroutineScope,
    private val redditPostRepository: RedditPostRepository,
    private val subReddit: String
) : ItemKeyedDataSource<String, RedditPost>(), CoroutineScope by coroutineScope {
    private var before: String? = null
    private var after: String? = null

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<RedditPost>) {
        load(
            subReddit = subReddit,
            size = params.requestedLoadSize,
            after = params.requestedInitialKey,
            callback = callback
        )
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<RedditPost>) {
        load(
            subReddit = subReddit,
            size = params.requestedLoadSize,
            after = this@RedditPostDataSource.after,
            callback = callback
        )
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<RedditPost>) {
        load(
            subReddit = subReddit,
            size = params.requestedLoadSize,
            before = this@RedditPostDataSource.before,
            callback = callback
        )
    }

    private fun load(
        subReddit: String,
        size: Int,
        before: String? = null,
        after: String? = null,
        callback: LoadCallback<RedditPost>
    ) {
        GlobalScope.launch(Dispatchers.IO) {
            val response = redditPostRepository.loadPosts(
                subReddit = subReddit,
                limit = size,
                after = after,
                before = before
            )
            val posts = response.children.map { it.data }
            this@RedditPostDataSource.before = response.before
            this@RedditPostDataSource.after = response.after
            callback.onResult(posts.expandFront(size)) // TODO Remove expand front
        }
    }

    private fun <T> List<T>.expandFront(size: Int): List<T> {
        val mutableList = toMutableList()
        val newCount = size - mutableList.size
        return if (newCount <= 0) {
            this
        } else {
            (0 until newCount).forEach { _ -> mutableList.add(mutableList.last()) }
            mutableList
        }
    }

    override fun getKey(item: RedditPost): String = item.id
}