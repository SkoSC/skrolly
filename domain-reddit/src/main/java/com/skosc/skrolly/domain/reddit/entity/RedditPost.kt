package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName

data class RedditPost(
    @SerializedName("preview")
    val preview: RedditPreview?,
    @SerializedName("name")
    val id: String
)