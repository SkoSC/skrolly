package com.skosc.skrolly.domain.reddit.misc

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode
import com.skosc.skrolly.domain.images.misc.ImageProviderNodeDeserializer
import com.skosc.skrolly.domain.reddit.entity.RedditImageProviderNode
import java.lang.reflect.Type

class RedditImageProviderNodeDeserializer : ImageProviderNodeDeserializer {
    companion object {
        private const val KEY_ID = "id"
        private const val KEY_TITLE = "title"
        private const val KEY_URL = "url"
    }

    override val type: Type = object : TypeToken<RedditImageProviderNode>() {}.type

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ImageProviderNode {
        val obj = json.asJsonObject
        return RedditImageProviderNode(
            obj[KEY_ID].asString,
            obj[KEY_TITLE].asString,
            obj[KEY_URL].asString
        )
    }

}