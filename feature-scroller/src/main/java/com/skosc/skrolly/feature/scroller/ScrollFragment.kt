package com.skosc.skrolly.feature.scroller


import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.lib.mvvm.viewModelProvider
import kotlinx.android.synthetic.main.feature_scroll_fragment_scroll.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

class ScrollFragment : Fragment(), KodeinAware {
    override val kodein: Kodein get() = (activity?.application as KodeinAware).kodein
    private val vm by viewModelProvider(ScrollViewModel::class)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.feature_scroll_fragment_scroll, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val adapter = FullImageRecyclerAdapter(requireContext(), createDisplayMetrics())
        recycler.adapter = adapter
        vm.images(arguments!!["providerType"] as ImageProviderType, arguments!!["node"] as String).observe(this, Observer { images ->
            adapter.submitList(images)
        })
    }

    private fun createDisplayMetrics(): DisplayMetrics {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics
    }
}
