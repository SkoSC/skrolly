package com.skosc.skrolly.feature.scroller

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.skosc.skrolly.domain.images.entitiy.Image
import com.skosc.skrolly.domain.reddit.entity.RedditImage
import com.skosc.skrolly.lib.android.SimpleDiffCallback

class FullImageRecyclerAdapter(context: Context, private val displayMetrics: DisplayMetrics) :
    PagedListAdapter<Image, FullImageRecyclerAdapter.ViewHolder>(SimpleDiffCallback()) {
    private val glide = Glide.with(context)

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.feature_scroll_item_full_image, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = getItem(position)
        require(image != null)
        glide.load(image.url)
            .fitCenter()
            .into(holder.image)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).hashCode().toLong()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.image)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Image>() {
            override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean = oldItem === newItem

            override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean = oldItem == newItem
        }
    }
}